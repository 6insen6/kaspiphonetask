﻿using OperatorDetector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class PhoneNumberTest
    {
        [Fact]
        public void TestInvalidParameterNumber()
        {
            PhoneNumber phone;
            var ex = Assert.Throws<ParseException>(() => phone = new PhoneNumber("89451234598"));
            Assert.Equal("Cant parse phone number. Invalid parameter number", ex.Message);
        } 
    }
}
