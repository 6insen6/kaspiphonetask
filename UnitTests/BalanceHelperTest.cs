using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using OperatorDetector.Database;
using OperatorDetector.Helpers;
using OperatorDetector.Models;
using OperatorDetector.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static OperatorDetector.Helpers.BalanceHelper;
using static OperatorDetector.Models.Resolver;

namespace UnitTests
{
    public class BalanceHelperTest
    {
        Mock<ILogger<BalanceHelper>> logger;
        Mock<IStringLocalizer<BalanceHelper>> localizer;
        BalanceHelper helper;
        DbContextOptions<ApplicationDbContext> options;
        Mock<ServiceResolver> serviceResolver;

        public BalanceHelperTest()
        {
            logger = new Mock<ILogger<BalanceHelper>>();
            localizer = new Mock<IStringLocalizer<BalanceHelper>>();
            options = new DbContextOptionsBuilder<ApplicationDbContext>()
                        .UseInMemoryDatabase(databaseName: "Database")
                        .Options;
            serviceResolver = new Mock<ServiceResolver>();

        }


        [Fact]
        public async Task BalanceHelperShouldReturnSuccess()
        {
            serviceResolver.Setup(x => x(It.IsAny<string>())).Returns(new ActivService());
            string key = "������ ��������";
            var localized = new LocalizedString(key, key);
            localizer.Setup(x => x[key]).Returns(localized);
            using (var context = new ApplicationDbContext(options))
            {
                PhoneNumber number = new PhoneNumber("+77011234556");
                helper = new BalanceHelper(serviceResolver.Object,
                        logger.Object, localizer.Object, context);

                var result = await helper.AddBalance(number, 500);

                Assert.True(result.IsSuccess);
            }
        }
    }
}