﻿using Microsoft.Extensions.Localization;
using OperatorDetector.Database;
using OperatorDetector.Models;
using OperatorDetector.Services;
using static OperatorDetector.Models.Resolver;

namespace OperatorDetector.Helpers
{
    public class BalanceHelper
    {

        private readonly ServiceResolver _serviceResolver;
        private readonly ILogger<BalanceHelper> _logger;
        private readonly IStringLocalizer<BalanceHelper> _localizer;
        private readonly ApplicationDbContext _context;

        public BalanceHelper(ServiceResolver serviceResolver,
            ILogger<BalanceHelper> logger,
            IStringLocalizer<BalanceHelper> localizer,
            ApplicationDbContext context)
        {
            _serviceResolver = serviceResolver;
            _logger = logger;
            _localizer = localizer;
            _context = context;
        }


        public async Task<ServiceResult> AddBalance(PhoneNumber phone, int amount)
        {
            var service = _serviceResolver(phone.OperatorCode);

            ServiceResult fillResult = await service.Fill(phone.Number, amount);

            if (!fillResult.IsSuccess)
            {
                _logger.LogError("Balance fill error");
                return fillResult;
            }

            await _context.Payments.AddAsync(new Payment()
            {
                Phone = phone.NumberString,
                Sum = amount,             
            });

            await _context.SaveChangesAsync();

            return new ServiceResult(true, _localizer["Баланс пополнен"].Value);
        }
    }
}
