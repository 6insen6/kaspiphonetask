﻿using OperatorDetector.Models;

namespace OperatorDetector.Services
{
    public interface IBalanceFill
    {
         Task<ServiceResult> Fill(string phone, int amount);
    }
}
