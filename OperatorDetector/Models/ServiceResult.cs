﻿namespace OperatorDetector.Models
{
    public class ServiceResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public ServiceResult()
        {
            IsSuccess = false;
        }        
        public ServiceResult(string message)
        {
            IsSuccess = false;
            this.Message = message;
        }      
        public ServiceResult(bool success, string message)
        {
            IsSuccess = success;
            this.Message = message;
        }

        public void SetSuccess(string msg)
        {
            IsSuccess = true;
            Message = msg;
        }
        public void SetSuccess()
        {
            IsSuccess = true;
        }
        public void SetError(string msg)
        {
            IsSuccess = false;
            Message = msg;
        }

        public void SetError()
        {
            IsSuccess = false;
        }
    }
}
