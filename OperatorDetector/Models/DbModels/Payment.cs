﻿using System.ComponentModel.DataAnnotations;

namespace OperatorDetector.Models
{
    public class Payment
    {
        public int Id { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        public int Sum { get; set; }
        public DateTime PaymentTime { get; private set; } = DateTime.Now;
    }
}
