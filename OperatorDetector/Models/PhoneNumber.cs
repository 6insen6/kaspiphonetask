﻿using System.Text.RegularExpressions;

namespace OperatorDetector.Models
{
    public class PhoneNumber
    {
        private static readonly Regex PhoneRegEx = new(@"^(?<prefix>\+7|8)(?<code>7\d{2})(?<number>\d{7})$", RegexOptions.Compiled);
        private string numberString;

        public string NumberString { get => numberString; private set => numberString = value.Trim(); }
        public string OperatorCode { get; private set; }
        public string Number { get; private set; }
        private Match Match { get; set; }

        public PhoneNumber(string number)
        {
            if (!IsPhoneNumber(number))
            {
                throw new ParseException($"Cant parse phone number. Invalid parameter {nameof(number)}");
            }

            NumberString = number;
            Match = PhoneRegEx.Match(NumberString);
            OperatorCode = Match.Groups["code"].Value;
            Number = Match.Groups["number"].Value;
        }

        private static bool IsPhoneNumber(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
            {
                return false;
            }

            return PhoneRegEx.Match(number).Success;
        }
    }
}
