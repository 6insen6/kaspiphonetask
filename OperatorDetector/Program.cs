using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using OperatorDetector.Database;
using OperatorDetector.ExceptionHandler;
using OperatorDetector.Helpers;
using OperatorDetector.Services;
using System.Globalization;
using static OperatorDetector.Models.Resolver;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
builder.Services.AddControllers();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddTransient(typeof(Lazy<>), typeof(Lazy<>));

builder.Services.AddScoped<ServiceResolver>(
    provider =>
        (type) =>
        type switch
        {
             "701" => provider.GetRequiredService<ActivService>(),
             "705" or "777" => provider.GetRequiredService<BeelineService>(),
             "707" or "747" => provider.GetRequiredService<Tele2Service>(),
             "700" or "708" => provider.GetRequiredService<AltelService>(),
            _ => throw new Exception("Service not found")
        }
    );
builder.Services.AddTransient<BalanceHelper>();
builder.Services.AddTransient<ActivService>();
builder.Services.AddTransient<BeelineService>();
builder.Services.AddTransient<Tele2Service>();
builder.Services.AddTransient<AltelService>();
builder.Services.AddTransient<ILogger, Logger<ExceptionHandler>>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseMiddleware<ExceptionHandler>();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

CultureInfo[] supportedCultures = new[]
            {
                new CultureInfo("ru"),
                new CultureInfo("kk"),
            };

app.UseRequestLocalization(new RequestLocalizationOptions
{
    DefaultRequestCulture = new RequestCulture("ru"),
    SupportedCultures = supportedCultures,
    SupportedUICultures = supportedCultures
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
