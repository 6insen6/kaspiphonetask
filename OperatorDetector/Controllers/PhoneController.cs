﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using OperatorDetector.DTO;
using OperatorDetector.Helpers;
using OperatorDetector.Models;
using System.Globalization;
using System.Text.RegularExpressions;

namespace OperatorDetector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private readonly BalanceHelper _helper;
        private readonly IStringLocalizer<PhoneController> _localizer;

        public PhoneController(BalanceHelper helper,
            IStringLocalizer<PhoneController> localizer)
        {
            _helper = helper;
            _localizer = localizer;
        }

        [HttpPost]
        public async Task<ActionResult> AddBalance(BalanceDto balance)
        {
            if (balance.Phone is null ||  string.IsNullOrEmpty(balance.Phone))
                return BadRequest(_localizer["Некорректный номер"].Value);
            if (balance.Sum <= 0)
            {
                return BadRequest(_localizer["Некорректная сумма пополнения"].Value);
            }

            PhoneNumber number = new(balance.Phone);

            ServiceResult balanceResult = await _helper.AddBalance(number, balance.Sum);

            return balanceResult.FromServiceResult();
        }
    }
}
