﻿using Microsoft.EntityFrameworkCore;
using OperatorDetector.Models;

namespace OperatorDetector.Database
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Payment> Payments { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
