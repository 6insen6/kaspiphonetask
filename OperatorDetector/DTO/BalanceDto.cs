﻿namespace OperatorDetector.DTO
{
    public class BalanceDto
    {
        public string? Phone { get; set; }
        public int Sum { get; set; }
    }
}
