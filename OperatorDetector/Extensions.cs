﻿using Microsoft.AspNetCore.Mvc;
using OperatorDetector.Models;

namespace OperatorDetector
{
    public static class ActionResultExtensions
    {
        public static ActionResult FromServiceResult(this ServiceResult serviceResult)
        {
            if (!serviceResult.IsSuccess)
                return new BadRequestObjectResult(serviceResult.Message);

            return new OkObjectResult(serviceResult.Message);
        }
    }
}
